/**
 * 李震
 * 我的码云：https://git.oschina.net/git-lizhen
 * 我的CSDN博客：http://blog.csdn.net/weixin_38215395
 * 联系：QQ1039953685
 */
#include "LineList.h"
#include <iostream>
#include <time.h>
using namespace std;

template <class T>
void Reverse_main(LineList<T> &m)
{
	m.Reverse();
}
void main(void)
{
	try{
		
		cout<<"原始空线性表：   "<<endl;
		LineList<int> L;
		cout<<"Length="<<L.Length()<<endl;
		cout<<"IsEmpty="<<L.IsEmpty()<<endl;
	
		cout<<endl<<"依次插入5个数据后：   "<<endl;
		L.Insert(0,2).Insert(1,5).Insert(2,3).Insert(3,1).Insert(4,-10);
		cout<<"List is "<<L<<endl;
		cout<<"IsEmpty="<<L.IsEmpty()<<endl;
		cout<<"Length="<<L.Length()<<endl;
		
		cout<<endl<<"测试Insert()：   "<<endl;
		L.Insert(1,10).Insert(2,100);
		cout<<"List is "<<L<<endl;
		cout<<"Length="<<L.Length()<<endl;
		
		cout<<endl<<"测试Delete()：   "<<endl;
		int x;
		L.Delete(1,x);
		cout<<"List is "<<L<<endl;
		cout<<"Length="<<L.Length()<<endl<<x<<endl;

		cout<<endl<<"测试Search()：   "<<endl;
		cout<<"List is "<<L<<endl;
		cout<<"5 in L? "<<L.Search(5)<<endl;
		cout<<"2 in L? "<<L.Search(2)<<endl;

		cout<<endl<<"测试Reverse()---偶数个数据：   "<<endl;
		L.Reverse();
		cout<<"List is "<<L<<endl;
		cout<<"Length="<<L.Length()<<endl;
		
		cout<<"测试Reverse()---奇数个数据：   "<<endl;
		L.Delete(1,x);
		cout<<endl<<"删除一个数据后：List is "<<L<<endl;
		L.Reverse();
		cout<<"List is "<<L<<endl;
		cout<<"Length="<<L.Length()<<endl;
		
		L.Insert(11,10);
		cout<<endl<<"测试Insert()：   "<<endl;
		cout<<"List is "<<L<<endl;
		cout<<"Length="<<L.Length()<<endl;  

		//反转测试
		for(int i=0;i<5000000;i++)
			L.Insert(i,i);
		cout<<"Length="<<L.Length()<<endl;
		
		//类的成员函数实现数据反转
		clock_t start_time,stop_time;
		start_time=clock();
		//计算执行时间	
		L.Reverse();
		stop_time=clock();
		cout<<"类的成员函数实现数据反转："<<float(stop_time-start_time)/CLK_TCK<<endl;

		//就地反转
		clock_t start_time_main,stop_time_main;
		start_time_main=clock();
		//计算执行时间	
		Reverse_main(L);
		stop_time_main=clock();
		cout<<"就地实现数据反转："<<float(stop_time_main-start_time_main)/CLK_TCK<<endl;

		//测试Half()函数
		//空数组
		cout<<"空数组:   "<<endl;
		L.Half();
		cout<<"List is "<<L<<endl;
		cout<<"IsEmpty="<<L.IsEmpty()<<endl;
		cout<<"Length="<<L.Length()<<endl;
		//有奇数个数
		cout<<endl<<"依次插入5个数据后：   "<<endl;
		L.Insert(0,2).Insert(1,5).Insert(2,3).Insert(3,1).Insert(4,-10);
		cout<<"List is "<<L<<endl;
		L.Half();
		cout<<"减半后：List is "<<L<<endl;
		cout<<"List is "<<L<<endl;
		cout<<"IsEmpty="<<L.IsEmpty()<<endl;
		cout<<"Length="<<L.Length()<<endl;
		//有偶数个数
		cout<<endl<<"依次插入第6个数据后：   "<<endl;
		L.Insert(1,9);	
		cout<<"List is "<<L<<endl;
		L.Half();
		cout<<"减半后：List is "<<L<<endl;
		cout<<"IsEmpty="<<L.IsEmpty()<<endl;
		cout<<"Length="<<L.Length()<<endl;

		//测试current
		for(int i=0;i<10;i++)
			L.Insert(i,i);
		cout<<endl<<"List is:   "<<L<<endl;

		L.Reset();    //置0

		//测试Next()函数
		L.Next();     //移至下一个元素
		cout<<"是否在最前？"<<L.Front()<<endl;
		int x;
		L.Current(x);
		cout<<"移至下一个元素后，当前元素:   "<<x<<endl;
		L.Next();     //移至下一个元素
		L.Current(x);
		cout<<"移至下一个元素后，当前元素:   "<<x<<endl;

		//测试Previous()函数
		L.Previous();     //移至上一个元素
		L.Current(x);
		cout<<"移至上一个元素后，当前元素:   "<<x<<endl;

		//测试End()函数
		for(int i=0;i<9;i++)
			L.Next();     //移至下一个元素
		L.Current(x);
		cout<<"后移九次后，当前元素:   "<<x<<endl;
		cout<<"是否在最后？"<<L.End()<<endl; 

		//测试Alternate()函数
		LineList<int> A,B;
		//产生测试数据
		for(int i=0;i<4;i++)    //生成等长的A和B
		{
			A.Insert(i,2*i);
			B.Insert(i,2*i-1);
		}
		int i=5;
		while(i>0)             //将B加长
		{
			B.Insert(4,10);
			i--;
		}
		cout<<"线性表A为：  "<<A<<endl;
		cout<<"线性表B为：  "<<B<<endl;
		//测试
		L.Alternate(A,B);
		cout<<"A在前，B在后，交叉组合后线性表L为：  "<<L<<endl;
		L.Alternate(B,A);
		cout<<"B在前，A在后，交叉组合后线性表L为：  "<<L<<endl;

		//测试Merge()函数
		LineList<int> A,B;
		//产生测试数据
		for(int i=0;i<4;i++)    //生成等长的A和B
		{
			A.Insert(i,2*i);
			B.Insert(i,2*i-1);
		}
		int i=5;
		while(i>0)             //将B加长
		{
			B.Insert(4,10);
			i--;
		}
		cout<<"线性表A为：  "<<A<<endl;
		cout<<"线性表B为：  "<<B<<endl;
		//测试
		L.Merge(A,B);
		cout<<"A在前，B在后，组合后线性表L为：  "<<L<<endl;
		L.Merge(B,A);
		cout<<"B在前，A在后，组合后线性表L为：  "<<L<<endl;

		//测试Split()函数
		//产生测试数据
		for(int i=0;i<10;i++)    //生成等长的A和B
		{
			L.Insert(i,i);
		}
		LineList<int> A,B;
		//原始线性表有偶数个数据
		L.Split(A,B);
		cout<<"原始线性表为：   "<<L<<endl;
		cout<<"分割后线性表A为：   "<<A<<endl;
		cout<<"分割后线性表B为：   "<<B<<endl;
		//原始线性表有奇数个数据
		L.Insert(10,10);
		L.Split(A,B);
		cout<<"原始线性表为：   "<<L<<endl;
		cout<<"分割后线性表A为：   "<<A<<endl;
		cout<<"分割后线性表B为：   "<<B<<endl;
	}
	catch(NoMem){
		cerr<<"No Memory!!!"<<endl;	
	}
	catch(OutOfRange){
		cerr<<endl<<"Out Of Range!!!"<<endl<<endl;
	}
}