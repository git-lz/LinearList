#include<iostream>
using namespace std;

//内存不足
class NoMem{
public:
	NoMem(){
		cout<<"内存不足"<<endl;
	}
	~NoMem(){};
};
//使new引发NoMemory异常而不是xalloc异常
void my_new_handler()
{
	throw NoMem();
}
new_handler Old_Handler_=set_new_handler(my_new_handler);

//超出范围
class OutOfRange{
public:
	OutOfRange(){};
	~OutOfRange(){};
};

